import java.util.*;

public class EncontroIndiceDeZero {

    private static int[] sequencia =
//                                      { 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1};
                                      { 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0};
//                                      { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
//                                      { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

    private static int[] tamanhoSequencia = new int[sequencia.length];          // armazena o tamanho das sequencias para cada posição do array sequencia

    public static void main(String[] args) {

        for (int posicao = 0; posicao < sequencia.length; posicao++) {

            /** Só adiciona o tamanho da no vetor tamanho da sequencia quando sequencia[posicao] = 0;
            *   usa o != para que a contagem das sequencias possa ocorrer quando posicao for a ultima o vertor sequencia[posicao]*/
             if (sequencia[posicao] != 1)
                tamanhoSequencia[posicao] = quantidadeUnsEsquerda(posicao) + quantidadeUnsDireita(posicao) + 1;
        }
        printPosição();
    }

    public static int quantidadeUnsEsquerda(int posicao) {

        int contaUmEsquerda = 0;
        int posicaoAnterior = posicao - 1;

        /**
         * Só entra no while a partir da posição 1 do vertor sequencia[1].
         * Não há necessidade de contar o número de uns à esquerda da primeira posição do vetor sequencia[0].
         * Só faz a contagem de uns à esquerda da posição se sequencia[posicaoAnterior] == 1
         */
        while (posicao > 0 && posicaoAnterior >= 0 && sequencia[posicaoAnterior] == 1) {
            contaUmEsquerda = contaUmEsquerda + 1;
            posicaoAnterior--;
        }
        return contaUmEsquerda;
    }

    public static int quantidadeUnsDireita(int posicao) {

        int contaUmDireita = 0;
        int proximaPosicao = posicao + 1;

        /**
         * Só faz contagem de uns à direita se a  variável posição for a última posição do vetor sequencia.
         * Obs.: a variável posicao só irá no máximo até sequencia.length - 1. Quando ela estiver nessa condição a
         * variável proximaPosicao será igual a posicao + 1. Nessas condições a expressão lógica dentro
         * proximaPosicao != sequencia.length, dentro do while, garantirá que o vetor sequencia[proximaPosicao]
         * não gere uma execção de ArrayIndexOutOfBoundsException.
         */

        while (posicao < sequencia.length && proximaPosicao != sequencia.length && sequencia[proximaPosicao] == 1) {
            contaUmDireita = contaUmDireita + 1;
            proximaPosicao++;
        }
        return contaUmDireita;
    }

    public static void printPosição() {

        int maior = 0;
        int posicao = 0;
        for (int i = 0; i < tamanhoSequencia.length; i++) {
            if (tamanhoSequencia[i] > maior) {
                maior = tamanhoSequencia[i];
                posicao = i + 1;
            }
        }
        System.out.println();
        System.out.println("Posição: " + posicao);
    }



}

























