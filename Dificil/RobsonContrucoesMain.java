import java.util.ArrayList;
import java.util.Scanner;

public class RobsonContrucoesMain {

    private static Scanner input = new Scanner(System.in);
    private static ArrayList<Double> cargos = new ArrayList<>();
    private static ArrayList<RobsonConstrucoesFuncionario> funcionarios = new ArrayList<>();


    public static void main(String[] args) {

        do {
            System.out.printf("%n====================================");
            System.out.printf("%n1. Cadastrar Cargo;");
            System.out.printf("%n2. Cadastrar Funcionário;");
            System.out.printf("%n3. Mostrar Relatório;");
            System.out.printf("%n4. Total Salário de Cargo;");
            System.out.printf("%n5. Relatorio Cargos;");

            System.out.printf("%n%nDigite sua Opção[1 - 4]: ");
            int opcao = input.nextInt();

            // So usuário digitar um valor fora do intervalo[1 -4]
            if ((opcao > 0 && opcao <= 5)) {
                switch (opcao) {
                    case 1:
                        cadastrarCargo();
                        break;
                    case 2:
                        cadastrarFuncionario();
                        break;
                    case 3:
                        mostrarRelatorio();
                        break;
                    case 4:
                        mostrarTotalPago();
                        break;
//                    case 5:
//                        mostraCargos();
//                        break;
                }
            } else {
                System.out.printf("%nOpção INVALIDA!%n");
            }
        } while (true);
    }

    public static void cadastrarCargo() {
        System.out.printf("%nDigite o valor do Sálario: ");
        cargos.add(input.nextDouble());
    }

    public static void cadastrarFuncionario() {

        // Cria um objeto da Classe RobsonConstrucoesFuncionario e referencia a variável novoFuncionario
        RobsonConstrucoesFuncionario novoFuncionario = new RobsonConstrucoesFuncionario();

        if (cargos.size() == 0) {
            System.out.printf("%nNÃO HÁ CARGOS CADASTRADOS.%nCADASTRE OS CARGOS PRIMEIRO!%n");
        } else {
            // Se o vetor de funcionários estiver vazio ele adiciona um novo fucionário ao vetor se checkar se o
            if (funcionarios.size() == 0) {

                System.out.printf("%nDigite o Código do Funcionário: ");
                novoFuncionario.setCondigoFuncionario(input.nextInt());                                                 // cadastra o código do novo funcionário no objeto criado
                input.skip("\\R");                                                                                      // Descarta a sequencia de escape \n quando usuário aperta Enter

                System.out.printf("Digite o Nome do Funcionário: ");
                novoFuncionario.setNome(input.nextLine());                                                              // cadastra o nome do novo funcionario no objeto criado

                System.out.printf("Digite o Codigo do Cargo do Funcionário: ");
                novoFuncionario.setCodigoCargo(validaCodigoCargo(input.nextInt()));                                     // cadastra o código do cargo do funcionário no objeto criado

                funcionarios.add(novoFuncionario);                                                                      // adiciona o fucionário ao vetor de funcionários que estava vazio

                // Se o vetor de funcionários não tiver vazio
            } else {

                System.out.printf("%nDigite o Código do Funcionário: ");
                novoFuncionario.setCondigoFuncionario(validaCodigoFuncionario(input.nextInt()));                        // cadastra o código do novo funcionário no objeto criado
                input.skip("\\R");                                                                                      // Descarta a sequencia de escape \n quando usuário aperta Enter

                System.out.printf("Digite o Nome do Funcionário: ");
                novoFuncionario.setNome(input.nextLine());

                System.out.printf("Digite o Codigo do Cargo do Funcionário: ");
                novoFuncionario.setCodigoCargo(validaCodigoCargo(input.nextInt()));

                funcionarios.add(novoFuncionario);
            }
        }
    }

    public static void mostrarRelatorio() {

        if (funcionarios.size() == 0) {
            System.out.printf("%nNÃO HÁ FUCIONÁRIOS CADASTRADOS!%n");
        } else {
            System.out.printf("%n==================== Relatório ==========================");

            for (int i = 0; i < funcionarios.size(); i++) {
                System.out.printf("%nNome: %s", funcionarios.get(i).getNome());
                System.out.printf("%nCódigo: %d", funcionarios.get(i).getCondigoFuncionario());
                System.out.printf("%nCódigo Cargo: %d%n", funcionarios.get(i).getCodigoCargo());
                System.out.printf("==============================================");
            }
        }

    }

    public static void mostrarTotalPago() {

        if (funcionarios.size() == 0) {
            System.out.printf("%nNÃO HÁ FUNCIONÁRIOS CADASTRADO!");
        } else {
            System.out.printf("%nQual cargo mostrar total: ");
            int codigoCargo = input.nextInt();
            int numFuncionarioNoCargo = 0;

            for (int i = 0; i < funcionarios.size(); i++) {

                if (funcionarios.get(i).getCodigoCargo() == codigoCargo) {
                    numFuncionarioNoCargo = numFuncionarioNoCargo + 1;
                }

            }
            System.out.println(numFuncionarioNoCargo);
            System.out.printf("%nTotal = %f", (numFuncionarioNoCargo * cargos.get(codigoCargo)));
        }

    }

    public static int validaCodigoCargo(int codigoCargo) {

        // Avisa o usário que não existe cargos criados e que o vetor de cargos está vazio
        if (cargos.size() == 0) {
            System.out.printf("%nNão exite cargo criado!");

        } else {
            while (!(codigoCargo >= 0 && codigoCargo < cargos.size())) {
                System.out.printf("%nCodigo invalido, digite um codigo válido: ");
                codigoCargo = input.nextInt();
            }
        }
        return codigoCargo;

    }

    public static int validaCodigoFuncionario(int codigoFuncionario) {

        boolean flag;

        do {

            flag = false;
            for (int i = 0; i < funcionarios.size(); i++) {
                if (codigoFuncionario == funcionarios.get(i).getCondigoFuncionario())
                    flag = true;
            }

            if (flag) {
                System.out.printf("%nCÓDIGO USADO POR OUTRO FUNCIONÁRIO, DIGITE OUTRO CÓDIGO: ");
                codigoFuncionario = input.nextInt();
            }

        } while (flag);

        return codigoFuncionario;
    }

    //    public static void mostraCargos() {
//        if (cargos.size() == 0) {
//            System.out.printf("%nNÃO EXISTE CARGOS CADASTRADO!");
//        } else {
//            for (int i = 0; i < cargos.size(); i++) {
//                System.out.printf("%nCargo(%d) = %f", i, cargos.get(i));
//            }
//        }
//    }

}



























