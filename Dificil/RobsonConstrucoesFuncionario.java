public class RobsonConstrucoesFuncionario {

    private String nome;
    private int condigoFuncionario;
    private int codigoCargo;


//    // Construtor
//    public RobsonConstrucoesFuncionario(String nome, int condigoFuncionario, int codigoCargo) {
//        this.nome = nome;
//        this.condigoFuncionario = condigoFuncionario;
//        this.codigoCargo = codigoCargo;
//    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCondigoFuncionario(int condigoFuncionario) {
        this.condigoFuncionario = condigoFuncionario;
    }

    public void setCodigoCargo(int codigoCargo) {
        this.codigoCargo = codigoCargo;
    }

    public String getNome() {
        return nome;
    }

    public int getCondigoFuncionario() {
        return condigoFuncionario;
    }

    public int getCodigoCargo() {
        return codigoCargo;
    }
}
