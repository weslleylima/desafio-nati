import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EncontreProdutoMaximo {

    public static void main(String[] args) {


        int[] arrayDeNum = {-6, 4, -5, 8, -10, 0, 8};

        List<Integer> arrayDeNegativo = new ArrayList<Integer>();
        List<Integer> arrayDePositivo = new ArrayList<Integer>();
        List<Integer> arrayDeProduto  = new ArrayList<Integer>();

        // ordena em ordem crescente o arrayDeNum
        Arrays.sort(arrayDeNum);

        // Separa os valores positivos e negativos do arrayDeNum
        for (int i = 0; i < arrayDeNum.length; i++) {
            if (arrayDeNum[i] < 0)
                arrayDeNegativo.add(arrayDeNum[i]);
            if (arrayDeNum[i] > 0)
                arrayDePositivo.add(arrayDeNum[i]);
        }

        // Se o número de inteiros negativo dentro do array for par
        if (arrayDeNegativo.size() % 2 == 0) {
            for (int i = 0; i < arrayDeNegativo.size(); i++) {
                arrayDeProduto.add(arrayDeNegativo.get(i));
            }
        } else  // Se o número de inteiros negativo dentro do array for ímpar

            // Se o número de inteiros negativo dentro do array for impar e diferente de 1
        if (arrayDeNegativo.size() != 1) {
            for (int i = 0; i < arrayDeNegativo.size(); i++) {
                arrayDeProduto.add(arrayDeNegativo.get(i));
            }
            // Remove o maior valor negativo para que o número de inteiros dentro do arrayDeProdutos se torne par
            arrayDeProduto.remove(Collections.max(arrayDeProduto));
        }

        for (int i : arrayDeProduto)
            System.out.printf("%d, ", i);


        for (int i : arrayDePositivo)
            System.out.printf("%d, ", i);
    }
}
