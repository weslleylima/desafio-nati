public class Xadrez {
    public static void main(String[] args) {

        /**
            Vazio  = 0,
            Peão   = 1,
            Bispo  = 2,
            Cavalo = 3,
            Torre  = 4,
            Rainha = 5,
            Rei    = 6
         */

        int[][] tabuleiro = {
                {4, 3, 2, 5, 6, 2, 3, 4},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {4, 3, 2, 5, 6, 2, 3, 0}
        };

        int[] numPecas = new int[7];

        for (int linha = 0; linha < 8; linha++) {
            for (int coluna = 0; coluna < 8; coluna++) {
                numPecas[tabuleiro[linha][coluna]]++;
            }
        }
        System.out.printf("%n%s: %d peça(s)", "Peão", numPecas[1]);
        System.out.printf("%n%s: %d peça(s)", "Bispo", numPecas[2]);
        System.out.printf("%n%s: %d peça(s)", "Cavalo", numPecas[3]);
        System.out.printf("%n%s: %d peça(s)", "Torre", numPecas[4]);
        System.out.printf("%n%s: %d peça(s)", "Rainha", numPecas[5]);
        System.out.printf("%n%s: %d peça(s)%n", "Rei", numPecas[6]);
    }
}
