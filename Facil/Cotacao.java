import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Cotacao {


    public static List<String> contacao = new ArrayList<>();
    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        int opcao = 0;

        do {
            System.out.printf("%n1. Adcionar");
            System.out.printf("%n2. Remover");
            System.out.printf("%n3. Alterar Titulo");
            System.out.printf("%n4. Imprimir Cotações%n");
            System.out.printf("%nDigite a Opção: ");
            opcao = input.nextInt();

            input.skip("\\R");

            switch (opcao) {
                case 1:
                    adiconar();
                    break;
                case 2:
                    remover();
                    break;
                case 3:
                    alterarTitulo();
                    break;
                case 4:
                    printContacoes();
                    break;
            }

        } while (true);
    }

    public static void adiconar() {

        System.out.printf("%nDigite a Contação para Adicionar: ");
        String titulo = input.nextLine();
        contacao.add(titulo);
    }

    public static void remover() {

        if (contacao.size() == 0) {
            System.out.printf("%nAdicione uma cotação!%n");
        } else {
            System.out.printf("%nDigite cotação a ser removida: ");
            String titulo = input.nextLine();

            for (int i = 0; i < contacao.size(); i++) {
                if (titulo.equals(contacao.get(i))) {
                    contacao.remove(i);
                }
            }
        }

    }

    public static void alterarTitulo() {

        if (contacao.size() == 0) {
            System.out.printf("%nAdicione uma cotação!%n");
        } else {
            System.out.printf("%nCotação à ser alterada: ");
            String titulo = input.nextLine();

            for (int i = 0; i < contacao.size(); i++) {
                if (titulo.equals(contacao.get(i))) {
                    System.out.printf("%nDigite novo titulo: ");
                    contacao.set(i, input.nextLine());
                }
            }
        }

    }

    public static void printContacoes() {

        if (contacao.size() == 0) {
            System.out.printf("%nAdicione uma cotação!");
        } else {
            for (int i = 0; i < contacao.size(); i++) {
                System.out.printf("%nCotação %d: %s", i+1, contacao.get(i));
            }
        }
        System.out.println();
    }
}





































