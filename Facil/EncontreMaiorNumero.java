import java.util.Scanner;
import java.lang.Math.*;


public class EncontreMaiorNumero {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int num1;
        int num2;
        int num;


        System.out.print("Digite o Primeiro Número: ");
        num1 = input.nextInt();

        System.out.print("Digite o Segundo Número: ");
        num2 = input.nextInt();

        num = Math.max(num1, num2);

        System.out.printf("%s%d", "Maior = ", num);

    }
}
